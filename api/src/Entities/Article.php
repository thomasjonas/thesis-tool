<?php

namespace Entities;

use \Doctrine\Common\Collections\ArrayCollection;
use JsonSerializable;

/**
 * Article model
 *
 * @Entity
 * @Table(name="article")
 */
class Article implements JsonSerializable
{

	/** @Id @Column(type="integer") @GeneratedValue */
	protected $id;
	
	/** @ManyToMany(targetEntity="Author", inversedBy="articles") */
	protected $author;
	
	/** @Column(type="string") */
	protected $title;

	/** @Column(type="text") */
	protected $html;

	/** @Column(type="text") */
	protected $source;

	/** @Column(type="text") */
	protected $plain;
	
	/** @Column(type="date") */
	protected $date;
	
	/** @OneToMany(targetEntity="Selection", mappedBy="article") */
	protected $selections = null;
	
	public function __construct(){
		$this->selections = new ArrayCollection();
	}

	function jsonSerialize() {
		$data = array(
			'id' => $this->getId(),
			'author' => $this->getAuthor(),
			'selections' => $this->getSelections(),
			'title' => $this->getTitle(),
			'html' => $this->getHtml(),
			'source' => $this->getSource(),
			'date' => $this->getDate()
		);
		
		return $data;
	}


	/**
	 * Get id
	 *
	 * @return integer 
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set title
	 *
	 * @param string $title
	 * @return Article
	 */
	public function setTitle($title)
	{
		$this->title = $title;
		return $this;
	}

	/**
	 * Get title
	 *
	 * @return string 
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * Set html
	 *
	 * @param text $html
	 * @return Article
	 */
	public function setHtml($html)
	{
		$this->html = $html;
		return $this;
	}

	/**
	 * Get html
	 *
	 * @return text 
	 */
	public function getHtml()
	{
		return $this->html;
	}

	/**
	 * Set plain
	 *
	 * @param text $plain
	 * @return Article
	 */
	public function setPlain($plain)
	{
		$this->plain = $plain;
		return $this;
	}

	/**
	 * Get plain
	 *
	 * @return text 
	 */
	public function getPlain()
	{
		return $this->plain;
	}

	/**
	 * Set date
	 *
	 * @param date $date
	 * @return Article
	 */
	public function setDate($date)
	{
		$this->date = $date;
		return $this;
	}

	/**
	 * Get date
	 *
	 * @return date 
	 */
	public function getDate()
	{
		return $this->date;
	}

	/**
	 * Set author
	 *
	 * @param Entities\Author $author
	 * @return Article
	 */
	public function setAuthor(\Entities\Author $author = null)
	{
		$this->author = $author;
		return $this;
	}

	/**
	 * Get author
	 *
	 * @return \Entities\Author 
	 */
	public function getAuthor()
	{
		return $this->author;
	}

	/**
	 * Add selections
	 *
	 * @param \Entities\Selection $selections
	 * @return Article
	 */
	public function addSelection(\Entities\Selection $selection)
	{
		$this->selections[] = $selection;
		return $this;
	}

	/**
	 * Get selections
	 *
	 * @return Doctrine\Common\Collections\Collection 
	 */
	public function getSelections()
	{
		return $this->selections;
	}

    /**
     * Set source
     *
     * @param string $source
     * @return Article
     */
    public function setSource($source)
    {
        $this->source = $source;
    
        return $this;
    }

    /**
     * Get source
     *
     * @return string 
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Remove selections
     *
     * @param Entities\Selection $selections
     */
    public function removeSelection(\Entities\Selection $selections)
    {
        $this->selections->removeElement($selections);
    }

    /**
     * Add author
     *
     * @param Entities\Author $author
     * @return Article
     */
    public function addAuthor(\Entities\Author $author)
    {
        $this->author[] = $author;
    
        return $this;
    }

    /**
     * Remove author
     *
     * @param Entities\Author $author
     */
    public function removeAuthor(\Entities\Author $author)
    {
        $this->author->removeElement($author);
    }
}