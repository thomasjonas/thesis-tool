<?php

namespace Entities;

use \Doctrine\Common\Collections\ArrayCollection;
use JsonSerializable;

/**
 * Author model
 *
 * @Entity
 * @Table(name="author")
 */
class Author implements JsonSerializable
{

	/** @Id @Column(type="integer", nullable=false) @GeneratedValue */
	protected $id;
	
	/** @Column(type="string") */
	protected $firstName;
	
	/** @Column(type="string") */
	protected $lastName;
	
	/** @ManyToMany(targetEntity="Article", mappedBy="author") */
	protected $articles = null;
	
	public function __construct(){
		$this->articles = new ArrayCollection();
	}

	function jsonSerialize() {
		$data = array(
			'id' => $this->getId(),
			'firstName' => $this->getFirstName(),
			'lastName' => $this->getLastName()
		);
		
		return $data;
	}

	/**
	 * Get id
	 *
	 * @return integer 
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set firstName
	 *
	 * @param string $firstName
	 * @return Author
	 */
	public function setFirstName($firstName)
	{
		$this->firstName = $firstName;
		return $this;
	}

	/**
	 * Get firstName
	 *
	 * @return string 
	 */
	public function getFirstName()
	{
		return $this->firstName;
	}

	/**
	 * Set lastName
	 *
	 * @param string $lastName
	 * @return Author
	 */
	public function setLastName($lastName)
	{
		$this->lastName = $lastName;
		return $this;
	}

	/**
	 * Get lastName
	 *
	 * @return string 
	 */
	public function getLastName()
	{
		return $this->lastName;
	}

	/**
	 * Add articles
	 *
	 * @param \Entities\Article $articles
	 * @return Author
	 */
	public function addArticle(\Entities\Article $articles)
	{
		$this->articles[] = $articles;
		return $this;
	}

	/**
	 * Get articles
	 *
	 * @return Doctrine\Common\Collections\Collection 
	 */
	public function getArticles()
	{
		return $this->articles;
	}

	/**
	 * Remove articles
	 *
	 * @param Entities\Article $articles
	 */
	public function removeArticle(\Entities\Article $articles)
	{
		$this->articles->removeElement($articles);
	}
}