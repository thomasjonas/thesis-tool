<?php

namespace Entities;

use \Doctrine\Common\Collections\ArrayCollection;
use JsonSerializable;

/**
 * Selection model
 *
 * @Entity
 * @Table(name="selection")
 */
class Selection implements JsonSerializable
{

	/** @Id @Column(type = "integer", nullable=false) @GeneratedValue */
	protected $id;
	
	/** @Column(type="text", nullable=true) */
	protected $selection;
	
	/** @Column(type="text") */
	protected $html;
	
	/** @ManyToOne(targetEntity="Article", inversedBy="selections") */
	protected $article;
	
	/** @ManyToMany(targetEntity="Tag", mappedBy="selections") */
	protected $tags = null;
	
	public function __construct(){
		$this->tags = new ArrayCollection();
	}
	
	function jsonSerialize() {
		$data = array(
			'id' => $this->getId(),
			'selection' => $this->getSelection(),
			'html' => $this->getHtml(),
			'article' => $this->getArticle()
		);
		
		return $data;
	}


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Selection
     *
     * @param text $Selection
     * @return Selection
     */
    public function setSelection($selection)
    {
        $this->selection = $selection;
        return $this;
    }

    /**
     * Get Selection
     *
     * @return text 
     */
    public function getSelection()
    {
        return $this->selection;
    }

    /**
     * Set plain
     *
     * @param text $plain
     * @return Selection
     */
    public function setHtml($html)
    {
        $this->html = $html;
        return $this;
    }

    /**
     * Get plain
     *
     * @return text 
     */
    public function getHtml()
    {
        return $this->html;
    }

    /**
     * Set article
     *
     * @param Entities\Article $article
     * @return Selection
     */
    public function setArticle(\Entities\Article $article = null)
    {
        $this->article = $article;
        return $this;
    }

    /**
     * Get article
     *
     * @return Entities\Article 
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * Add tags
     *
     * @param Entities\Tag $tags
     * @return Selection
     */
    public function addTag(\Entities\Tag $tags)
    {
        $this->tags[] = $tags;
        return $this;
    }

    /**
     * Get tags
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Remove tags
     *
     * @param Entities\Tag $tags
     */
    public function removeTag(\Entities\Tag $tags)
    {
        $this->tags->removeElement($tags);
    }
}