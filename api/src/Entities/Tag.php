<?php

namespace Entities;

use \Doctrine\Common\Collections\ArrayCollection;
use JsonSerializable;

/**
 * Tag model
 *
 * @Entity
 * @Table(name="tag", uniqueConstraints={@UniqueConstraint(name="tag_idx", columns={"value"})})
 */
class Tag implements JsonSerializable
{

	/** @Id @Column(type="integer", nullable=false) @GeneratedValue */
	protected $id;
	
	/** @Column(type="string") */
	protected $value;
	
	/** @ManyToMany(targetEntity="Selection", inversedBy="tags") */
	protected $selections = null;
	
	public function __construct(){
		$this->selections = new ArrayCollection();
	}
	
	function jsonSerialize() {
		$data = array(
			'id' => $this->getId(),
			'value' => $this->getValue()
		);
		
		return $data;
	}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return Tag
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Add Selections
     *
     * @param models\Selection $Selections
     * @return Tag
     */
    public function addSelection(\Entities\Selection $selections)
    {
        $this->selections[] = $selections;
        return $this;
    }

    /**
     * Get Selections
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getSelections()
    {
        return $this->selections;
    }

    /**
     * Remove Selections
     *
     * @param Entities\Selection $Selections
     */
    public function removeSelection(\Entities\Selection $selections)
    {
        $this->selections->removeElement($selections);
    }
}