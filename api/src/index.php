<?php

require_once "../vendor/autoload.php";
require_once "cli-config.php";

// create new Slim instance
$app = new \Slim\Slim(array('debug' => true));
$res = $app->response();
$res->header('Access-Control-Allow-Origin', '*');
$res->header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
$res->header('Content-Type', 'application/json');


// add new Route 
$app->get("/", function () use ($app, $em) {
	echo "<h1>Hello Thesis API</h1>";
});

$app->get("/tags", function () use ($app, $em) {
	$dql = "SELECT t FROM Entities\Tag t ORDER BY t.value";
	$query = $em->createQuery($dql);
	$tags = $query->getResult();
	echo json_encode($tags);
});

$app->post("/tags", function () use ($app, $em) {
	$body = $app->request()->getBody();
	$body = json_decode($body);
	$tag = new Entities\Tag();
	$tag->setValue($body->value);
	if(isset($body->selectionId)){
		$selection = $em->find('Entities\Selection', $body->selectionId);
		$tag->addSelection($selection);
		$selection->addTag($tag);
		$em->persist($selection);
	}
	$em->persist($tag);
	$em->flush();
	echo json_encode($tag);
});

$app->put("/tags/:id", function ($id) use ($app, $em) {
	$tag = $em->find('Entities\Tag', $id);
	$body = $app->request()->getBody();
	$body = json_decode($body);
	if(isset($body->selectionId)){
		$selection = $em->find('Entities\Selection', $body->selectionId);
		$tag->addSelection($selection);
		$selection->addTag($tag);
		$em->persist($selection);
	}
	$em->persist($tag);
	$em->flush();
	echo json_encode($tag);
});

$app->post("/selections", function() use ($app, $em){
	$selection = new Entities\Selection();
	$body = $app->request()->getBody();
	$body = json_decode($body);
	$article = $em->find('Entities\Article', $body->article_id);
	$selection->setArticle($article);
	$selection->setHtml($body->html);
	$selection->setSelection(strip_tags($body->html));
	$article->addSelection($selection);
	$em->persist($article);
	$em->persist($selection);
	$em->flush();
	echo json_encode($selection);
});


$app->delete("/selections/:id", function ($id) use ($app, $em) {
	$selection = $em->find('Entities\Selection', $id);
	$em->remove($selection);
	$em->flush();
});

$app->put("/selections/:id", function ($id) use ($app, $em) {
	$selection= $em->find('Entities\Selection', $id);
	$body = $app->request()->getBody();
	$body = json_decode($body);
	$selection->setHtml($body->html);
	$selection->setSelection(strip_tags($body->html));
	if(isset($body->removeTag)){
		$tag = $em->find('Entities\Tag', $body->removeTag);
		$tag->removeSelection($selection);
		$selection->removeTag($tag);
		$em->persist($tag);
	}
	$em->persist($selection);
	$em->flush();
	echo json_encode($selection);
});

$app->get("/authors", function () use ($app, $em) {
	$dql = "SELECT a FROM Entities\Author a ORDER BY a.firstName, a.lastName";
	$query = $em->createQuery($dql);
	$authors = $query->getResult();
	echo json_encode($authors);
});

$app->get("/articles", function () use ($app, $em) {
	/*
$qb = $em->createQueryBuilder();
	$qb->select('a.id, a.date, a.source, a.title, b')
		->from('Entities\Article', 'a')
		->leftJoin('a.author', 'b')
		->addSelect('a')
		->addSelect('b')
		->orderBy('a.date');
*/

	$dql = "SELECT partial art.{id, title, date}, auth FROM Entities\Article art
			LEFT JOIN art.author auth
			ORDER BY art.date ASC";
	$query = $em->createQuery($dql);
	$articles = $query->getArrayResult();

/*
	$query = $qb->getQuery();
	$articles = $query->getArrayResult();
*/
	echo json_encode($articles);
});

$app->get("/articles/:id", function ($id) use ($app, $em) {
	$dql = "SELECT art, auth, sel, tags FROM Entities\Article art
			LEFT JOIN art.author auth
			LEFT JOIN art.selections sel
			LEFT JOIN sel.tags tags
			WHERE art.id = ?1";
	$query = $em->createQuery($dql);
	$query->setParameter(1, $id);
	$article = $query->getArrayResult();
	$article = $article[0];
	echo json_encode($article);
});

$app->put("/articles/:id", function ($id) use ($app, $em) {
	$article= $em->find('Entities\Article', $id);
	$body = $app->request()->getBody();
	$body = json_decode($body);
	$article->setTitle($body->title);
	$html = html_entity_decode($body->html);
	$html = parse_html($html, $body->source);
	$html = str_replace(array("\n", "\r", "\t"), array("", "", ""), $html);
	$article->setHtml($html);
	$article->setDate(date_create($body->date->date));
	$article->setSource($body->source);
	
	foreach($body->author as $a){
		$authorId = 0;
		if(isset($a->id)) {
			$authorId = $a->id;
		}
		$author = $em->find('Entities\Author', $authorId);
		if(count($author) < 1){
			$author = new Entities\Author();
			$author->setFirstName($a->firstName);
			$author->setLastName($a->lastName);
			$em->persist($author);
		}
		$article->removeAuthor($author);
		$article->addAuthor($author);
	}
	
	
	foreach($body->selections as $selection) {
		$selObj = $em->find('Entities\Selection', $selection->id);
		$selObj->setHtml($selection->html);
		$selObj->setSelection(strip_tags($selection->html));
		$article->addSelection($selObj);
		$em->persist($selObj);
	}
	$em->persist($article);
	$em->flush();
	
	$dql = "SELECT art, auth, sel FROM Entities\Article art
			LEFT JOIN art.author auth
			LEFT JOIN art.selections sel
			WHERE art.id = ?1";
	$query = $em->createQuery($dql);
	$query->setParameter(1, $id);
	$article = $query->getArrayResult();
	echo json_encode($article[0]);	
});

$app->delete("/articles/:id", function ($id) use ($app, $em) {
	$article = $em->find('Entities\Article', $id);
	$em->remove($article);
	$em->flush();
});

$app->post("/articles", function () use ($app, $em) {
	$article = new Entities\Article();
	$body = $app->request()->getBody();
	$body = json_decode($body);
	$article->setTitle($body->title);
	$html = html_entity_decode($body->html);
	$article->setHtml($html);
	$article->setPlain(strip_tags($body->html));
	$article->setDate(date_create($body->date->date));
	$article->setSource($body->source);

	foreach($body->author as $a){
		$authorId = 0;
		if(isset($a->id)) {
			$authorId = $a->id;
		}
		$author = $em->find('Entities\Author', $authorId);
		if(count($author) < 1){
			$author = new Entities\Author();
			$author->setFirstName($a->firstName);
			$author->setLastName($a->lastName);
			$em->persist($author);
		}
		$article->addAuthor($author);
	}
	
	$em->persist($article);
	$em->flush();
	
	$dql = "SELECT art, auth, sel FROM Entities\Article art
			LEFT JOIN art.author auth
			LEFT JOIN art.selections sel
			WHERE art.id = ?1";
	$query = $em->createQuery($dql);
	$query->setParameter(1, $article->getId());
	$article = $query->getArrayResult();
	echo json_encode($article[0]);
});

$app->post("/strip", function () use ($app) {
	require '../vendor/readability/Readability.inc.php';
	$url = $app->request()->post('url');
	// Get cURL resource
	$curl = curl_init();
	// Set some options - we are passing in a useragent too here
	curl_setopt_array($curl, array(
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_URL => $url,
		CURLOPT_USERAGENT => 'New Aesthetic Research (THOMASJONAS)'
	));
	// Send the request & save response to $resp
	$html = curl_exec($curl);
	// Close request to clear up some resources
	curl_close($curl);
	preg_match("/charset=([\w|\-]+);?/", $html, $match);
	$charset = isset($match[1]) ? $match[1] : 'utf-8';

	$readability = new Readability($html, $charset);
	$data = $readability->getContent();
	$return['title'] = $data['title'];	
	$return['html'] = parse_html($data['content'], $url);;
	$return['html'] = str_replace(array("\n", "\r", "\t"), array("", "", ""), $return['html']);
	$return['source'] = $url;
	echo json_encode($return);
});

// run the Slim app
$app->run();

function parse_html($html, $url){
	$html = html_entity_decode($html);
	$encoding = mb_detect_encoding($html);
	$html = $encoding ? @iconv($encoding, 'UTF-8', $html) : $html;

	// Insert a head and meta tag immediately after the opening <html> to force UTF-8 encoding
	$insertPoint = false;
	if (preg_match("/<html.*?>/is", $html, $matches, PREG_OFFSET_CAPTURE)) {
	    $insertPoint = mb_strlen( $matches[0][0] ) + $matches[0][1];
	}
	if ($insertPoint) {
		$html = mb_substr($html,0,$insertPoint) 
				. "<head><meta http-equiv='Content-type' content='text/html; charset=UTF-8' /></head>" 
				. mb_substr($html, $insertPoint);
	}else{
		$html = "<head><meta http-equiv='Content-type' content='text/html; charset=UTF-8' /></head>".$html;
	}

	$doc = new DOMDocument();
	@$doc->loadHTML($html);

	$parts = parse_url($url);
	$base = $parts['scheme'].'://'.$parts['host'].'/';

	$xpath = $xpath = new DOMXpath($doc);
	$nodes = $xpath->query("//img");
	foreach ($nodes as $node) {
		$src = $node->getAttribute('src');
		if(strpos($src, $base) !== 0 && substr($src, 0, 4) != 'http'){
			$node->setAttribute('src', $base.$src);
		}
		$node->removeAttribute('width');
		$node->removeAttribute('height');
	}

	$xpath = new DOMXpath($doc);
	$nodes = $xpath->query("//p");
	foreach ($nodes as $node) {
		$trimmed_value = trim($node->nodeValue);                       
		if (empty($trimmed_value)) {
			$node->parentNode->removeChild($node);
		}else {
			break;
		}
	}

	$html = $doc->saveHTML();
	$html = strip_tags($html, '<p><a><b><strong><i><em><ul><li><ol><blockquote><br/><img><iframe>');
	$pattern = "/<p[^>]*><\\/p[^>]*>/"; 
	$html = preg_replace($pattern, '', $html);
	return $html;
}