define([
	"backbone",
	"models/Article"
],
function(Backbone, Article) {
	
	var ArticleCollection = Backbone.Collection.extend({
		model: Article,
		url: '/api/src/articles'
	});

	return ArticleCollection;

});