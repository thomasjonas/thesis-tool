define([
	"backbone",
	"models/Author"
],
function(Backbone, Author) {
	
	var AuthorCollection = Backbone.Collection.extend({
		model: Author,
		url: '/api/src/authors'
	});

	return AuthorCollection;

});