define([
	"backbone",
	"models/Selection"
],
function(Backbone, Author) {
	
	var SelectionCollection = Backbone.Collection.extend({
		model: Selection,
	});

	return SelectionCollection;

});