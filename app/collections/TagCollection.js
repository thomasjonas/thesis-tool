define([
	"backbone",
	"models/Tag"
],
function(Backbone, Tag) {
	
	var TagCollection = Backbone.Collection.extend({
		model: Tag,
		url: '/api/src/tags'
	});

	return TagCollection;

});