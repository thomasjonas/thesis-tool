// Set the require.js configuration for your application.
require.config({

	paths: {
		// JavaScript folders.
		libs: "../assets/js/libs",
		plugins: "../assets/js/plugins",
		vendor: "../assets/vendor",

		// Libraries.
		jquery: "../assets/js/libs/jquery",
		jqueryui: "../assets/vendor/jqueryui/js/jquery-ui-1.9.1.custom",
		underscore: "../assets/js/libs/underscore",
		backbone: "../assets/js/libs/backbone",
		handlebars: "../assets/js/libs/handlebars-1.0.0.beta.6",
		json2: "../assets/js/libs/json2",
		i18nprecompile: "../assets/js/libs/i18nprecompile",

		// Plugins
		hbs: "../assets/js/plugins/hbs",
/* 		handlebars_helpers: "../assets/js/helpers", */
		wysihtml5 : "../assets/vendor/wysihtml5/js/wysihtml5",
		wysihtml5_parser : "../assets/vendor/wysihtml5/js/parser_rules",
		rangy : "../assets/js/plugins/rangy/rangy-core",
		rangy_cssclassapplier : "../assets/js/plugins/rangy/rangy-cssclassapplier",
		rangy_textrange : "../assets/js/plugins/rangy/rangy-textrange",
		
		// vendor
		bootstrap: "../assets/vendor/foundation/js/modernizr.foundation" 

	},

	shim: {
		// Backbone library depends on lodash and jQuery.
		backbone: {
			deps: ["underscore", "jquery"],
			exports: "Backbone"
		},

		// Handlebars has no dependencies.
		handlebars: {
			exports: "Handlebars"
		},
		
		hbs: {
			deps: ['handlebars']
		},

		json2 : {
			exports : 'JSON'
		},

/* 		handlebars_helpers: ['handlebars'], */

		jqueryui : {
			deps : ['jquery']
		},

		wysihtml5 : {
			deps : ['wysihtml5_parser']
		},
		
		bootstrap: ["jquery"]
	},
	deps: ["jquery", "underscore", "jqueryui", "backbone"]
});

require([
	"router",
	"handlebars"
],

function(Router, Handlebars) {

	$(function(){

		var json_articles;
		$.getJSON('/api/src/articles', function(data){
			var app = new Router({ json: data });
			window.app = app;
			Backbone.history.start({ pushState: true});
		});
		
	});

	$(document).on("click", "a:not([data-bypass])", function(evt) {
		var href = { prop: $(this).prop("href"), attr: $(this).attr("href") };
		var root = location.protocol + "//" + location.host;
		if (href.prop && href.prop.slice(0, root.length) === root) {
			evt.preventDefault();
			Backbone.history.navigate(href.attr, true);
		}
	});
});

