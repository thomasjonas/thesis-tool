define([
	"backbone",
	"models/Selection",
	"models/Author",
	"collections/SelectionCollection",
	"collections/AuthorCollection"
],
function(
	Backbone,
	Selection,
	Author,
	SelectionCollection,
	AuthorCollection
) {
	
	var Article = Backbone.Model.extend({ 
		initialize: function(){
			_.bindAll(this);
			this.set('selections', new SelectionCollection());
			this.set('author', new AuthorCollection());
			var date = this.get('date');
			if( date !== undefined) {
				this.setTimestamp();
			}
		},
		
		setTimestamp: function(){
			var date = this.get('date');
			var dateObject = new Date(date.date);
			var months = ["January", "Februari", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
			var string = dateObject.getDate() + ' ' + months[dateObject.getMonth()] + ' ' + dateObject.getFullYear();
			this.set('timestamp', string);
		},
		
		parse: function(response) {
			if(response.selections !== undefined) {	
				var collection = new SelectionCollection();
				if(this.attributes.selections !== undefined){
					collection = this.get('selections');
				}
				var tmp = [];
				_.each(response.selections, function(sel){
					var selection = new Selection(sel);
					tmp.push(selection);
				});
				collection.reset(tmp);
				response.selections = collection;
				this.trigger('change:selections');
			}
			if( response.author !== undefined ) {
				var collection = new AuthorCollection();
				if(this.attributes.author !== undefined){
					collection = this.get('author');
				}
				var tmp = [];
				_.each(response.author, function(auth){
					var author = new Author(auth);
					tmp.push(author);
				});
				collection.reset(tmp);
				response.author = collection;
			}
			this.setTimestamp();
			return response;	
		},
		
		toJSON: function(options) {
			var json = _.clone(this.attributes);
			json.author = this.get('author').toJSON();
			return json;
      	},
	});

	return Article;

});