define([
	"backbone",
	"models/Tag",
	"collections/TagCollection"
],
function(Backbone, Tag, TagCollection) {
	
	var Selection = Backbone.Model.extend({ 
		url: function(){ 
			if(this.isNew()){
				return "/api/src/selections";
			}else{
				return "/api/src/selections/" +this.get("id");
			}
		},
		
		initialize: function() {
			_.bindAll(this);
			var tags = new TagCollection();
			if( this.get('tags') !== undefined) {
				_.each(this.get('tags'), function(tag) {
					var t = new Tag(tag);
					tags.add(t);
				}, this);
			}
			this.set('tags', tags);
			tags.on('remove', this.removeTag);
			tags.on('add', this.addTag);
		},
		
		addTag: function(tag) {

		},
		
		removeTag: function(tag) {
			this.set('removeTag', tag.get('id'));
			this.save();
		}
		
/*
		parse: function(response) {
			if(response.tags !== undefined) {	
				/*
var collection = new SelectionCollection();
				if(this.attributes.selections !== undefined){
					collection = this.get('selections');
				}
				var tmp = [];
				_.each(response.selections, function(sel){
					var selection = new Selection(sel);
					tmp.push(selection);
				});
				collection.reset(tmp);
				response.selections = collection;
				this.trigger('change:selections');

			}
			return response;	
		}
*/
	});

	return Selection;

});