define([
	// Application.
	"backbone",
	"models/Article",
	"collections/ArticleCollection",
	"collections/AuthorCollection",
	"collections/TagCollection",
	"views/SidebarList",
	"views/FormView",
	"views/ScraperView",
	"views/TooltipView"
],
function(
	Backbone,
	Article,
	ArticleCollection,
	AuthorCollection,
	TagCollection,
	SidebarList,
	FormView,
	ScraperView,
	TooltipView
){

	// Defining the application router, you can attach sub routers here.
	var Router = Backbone.Router.extend({

		initialize: function(options){
			this.articles = new ArticleCollection(options.json);
			this.authors = new AuthorCollection();
			this.authors.fetch();
			this.sidebar = new SidebarList({collection: this.articles});
			this.tooltip = new TooltipView();
			this.tags = new TagCollection();
			this.tags.fetch();
			this.on("external_read", this.new_form);
		},

		routes: {
			""					: "index",
			"article/new"		: "new_article",
			"article/edit/:id"	: "edit_article"
		},

		index: function() {

		},

		new_article: function(){
			if(this.containerView !== undefined) this.containerView.empty();
			this.containerView = new ScraperView();
			$("#content").html(this.containerView.render().$el);
		},

		edit_article: function(id) {
			article = this.articles.get(id);
			article.fetch();
			if(this.containerView !== undefined) this.containerView.empty();
			this.containerView = new FormView({model: article});
			$("#content").html(this.containerView.render().$el);
			this.containerView.read();
		},
			
		new_form: function(data){
			article = new Article(data);
			if(this.containerView !== undefined) this.containerView.empty();
			this.containerView = new FormView({model: article});
			$("#content").html(this.containerView.render().$el);
			this.containerView.edit();
		}
	});

	return Router;

});
