define(function(require){
	"use strict";
	return {
		Article 				: require('hbs!templates/Article'),
		ArticleList 			: require('hbs!templates/ArticleList'),
		FormView				: require('hbs!templates/FormView'),
		ScraperView				: require('hbs!templates/ScraperView'),
		TooltipView				: require('hbs!templates/TooltipView'),
	};
});