define([
	'backbone',
	'templates'
], 
function (Backbone,templates) {
	"use strict";

	var ArticleView = Backbone.View.extend({
		className: "item",
		template: templates.Article,

		initialize: function(){
			_.bindAll(this, "render");
			this.model.bind('change', this.render);
			this.render();
		},

		render: function(){
			this.$el.html(this.template(this.model.toJSON()));
			return this;
		}
	});

	return ArticleView;
});