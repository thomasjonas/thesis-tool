define([
	'backbone',
	'templates',
	'views/SelectionView',
	'models/Author',
	'wysihtml5',
/* 	'handlebars_helpers' */
],
function(
	Backbone, 
	templates,
	SelectionView,
	Author
){

	var FormView = Backbone.View.extend({
		template: templates.FormView,
		
		events: {
			"change .author" : "changeAuthor",
			"blur .author" : "changeAuthor",
			"click #read_btn" : "read",
			"click #source_btn" : "source",
			"click #edit_btn" : "edit",
			"click #html_btn" : "edit",
			"click #save_btn" : "saveModel",
			"click #remove_btn" : "removeModel"
		},
		
		initialize: function(){
			_.bindAll(this);
			this.model.on("change:html", this.render);
			this.model.on("change:id", this.navigate);
/* 			this.model.on("change:html", this.doWysi); */
			this.selectionView = new SelectionView({model: this.model});
			this.render();
		},

		render: function(){
			if(this.model !== undefined){
				this.$el.html(this.template(this.model.toJSON()));
				this.doDatePicker();
				this.doAuthorComplete();
				this.doWysi();
			}
			return this;
		},
		
		navigate: function(article){
			app.navigate('/article/edit/'+article.get('id'));
		},
				
		doWysi: function(){
			var that = this;
			var body = this.$el.find('.body');
			if(this.editor !== undefined){
				this.editor.off();
				$("iframe.wysihtml5-sandbox, input[name='_wysihtml5_mode']").remove();
				$("body").removeClass("wysihtml5-supported");
				this.editor = undefined;
			}
			
			var textarea = this.$el.find('#html').get(0);
			if (textarea !== undefined) {
				$(textarea).height(textarea.scrollHeight);
				this.editor = new wysihtml5.Editor(textarea, { // id of textarea element
					toolbar:      "toolbar", // id of toolbar element
					parserRules:  wysihtml5ParserRules, // defined in parser rules set
					stylesheets: ['/assets/css/editor.css']
				});
				var iframe = this.$el.find('.wysihtml5-sandbox');
				var that = this;
				this.editor.on("load", function(){
					setTimeout(function(){
						if ( iframe.get(0).contentWindow != undefined ) {
							var html = that.selectionView.updateSelections();
							that.editor.setValue($('.body').html(), true);
							iframe.height($('.body').outerHeight()+300);
							$("#html").height($('.body').outerHeight());
						}
					}, 200);
				});
			}
		},
				
		doDatePicker: function(){
			var that = this;
			this.$el.find("#input_date").datepicker("destroy");
			this.$el.find("#input_date").datepicker({
				dateFormat: "yy-mm-dd",
				onSelect: function(dateText, obj){
					var date = that.model.get('date') || {};
					date.date = dateText;
				}
			});	
		},
		
		doAuthorComplete: function(){
			this.$el.find(".author").each(function(index, field) {			
				$(field).autocomplete({ 
					source: function(request, response) {
						var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
						response( $.grep( app.authors.toJSON(), function( value ) {
							return ( matcher.test( value.firstName ) || matcher.test( value.lastName ) || matcher.test( value.firstName + ' ' + value.lastName ) );
						}) );
					},
					select: function( event, ui ) {
						$(event.target).val(ui.item.firstName + ' ' + ui.item.lastName).prev().val(ui.item.id);
						return false;
					} 
				}).data( "autocomplete" )._renderItem = function( ul, item ) {
					return $( "<li>" )
					.data( "item.autocomplete", item )
					.append( "<a>" + item.firstName + " " + item.lastName + "</a>" )
					.appendTo( ul );
				};
			});
		},
		
		read: function(){
			this.$el.removeClass('edit');
			this.$el.removeClass('source');
			this.$el.addClass('read');
			if (this.prevView == 'html' || this.prevView == 'wysi' ) {
				$('.body').html(this.editor.getValue());
			}
			this.selectionView.updateSelections();
			this.prevView = 'read';
		},
		
		edit: function(evt){			
			this.$el.removeClass('read');
			this.$el.removeClass('source');
			this.$el.addClass('edit');
		
			if( this.editor !== undefined ) {
				if( this.prevView == 'html' ){
					$('.body').html(this.textarea.getValue());
				} else if( this.prevView == 'wysi' ){
					$('.body').html(this.composer.getValue());
				}
				this.selectionView.updateSelections();
				this.editor.setValue($('.body').html());
			}
			
			var that = this;
			this.$el.find("#input_date").datepicker({
				dateFormat: "yy-mm-dd",
				onSelect: function(dateText, obj){
					var date = that.model.get('date') || {};
					date.date = dateText;
				}
			});
			
			if(evt !== undefined && evt.target.id == "html_btn") {
				this.prevView = 'html';
			} else {
				this.prevView = 'wysi';
			}
		},
		
		source: function(){
			this.$el.removeClass('edit');
			this.$el.removeClass('read');
			this.$el.addClass('source');
			var iframe = this.$el.find(".source > iframe");
			if( iframe.attr('src') !== this.model.get('source')) {
				iframe.attr('src', this.model.get('source'));
			}
		},
		
		saveModel: function(){
			$('.body').html(this.editor.getValue(false));
			this.selectionView.updateSelections();
			var isNew = this.model.isNew();
			var date = this.model.get('date') || {};
			date.date = $("#input_date").val();
			var authors = this.model.get('author');

			var author = authors;
			if ( this.editor !== undefined ) {
				$("#html").html($.trim(this.editor.getValue(false)));
			}
			var html = this.$el.find('#html');
			html.find('.marked').contents().unwrap();
			html = $.trim(html.html());
			
			this.model.set({
				title: $("#input_title").val(),
				author: author,
				date: date,
				html: html,
				source: $("#input_source").val()
			});
			
			if(isNew) app.articles.add(this.model);
			this.model.save();	
		},
		
		removeModel: function(){
			this.model.destroy();
			this.empty();
		},

		changeAuthor: function(event){
			var val = $.trim($(event.target).val());
			if( val !== undefined && val !== "" ) {
				var split = val.split(' ');
				var found = app.authors.where({firstName: split[0], lastName: split[1]});
				var a = new Author();
				if(found.length == 1){
					a = found[0];
					if ( !a.isNew() ){
						$(event.target).prev().val(found[0].get('id'));
					} else{
						$(event.target).prev().val('');					
					}
				}else{
					a = new Author({ firstName: split[0], lastName: split[1]});
					app.authors.add(a);
					$(event.target).prev().val('');
				}
				var authors = this.model.get('author');
				authors.add(a);
				this.render();
			}
		},

		empty: function(){
			this.selectionView.empty();
			this.off();
			this.$el.empty();
		}

	});

	return FormView;
});