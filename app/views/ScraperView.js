define([
	'backbone',
	'templates'
],
function(
	Backbone, 
	templates
){

	var ScraperView = Backbone.View.extend({
/* 		el: $("#content"), */
		template: templates.ScraperView,
		
		events: {
			"click #submit_url" : "scrape"
		},

		
		initialize: function(){
			console.log('scraper view initialized');
			_.bindAll(this);
			this.render();
		},
		
		render: function(){
			this.$el.html(this.template());
			return this;
		},
		
		scrape: function(){
			console.log('scrape!');
			var url = $("#source_url").val();
			$.ajax({
				url : '/api/src/strip',
				data: { url: url},
				success: function(data){
					app.trigger("external_read", data);
				},
				type: 'POST',
				dataType: 'json'
			});
		},
				
		empty: function(){
			console.log('scraper view emptied');
			this.off();
			this.$el.empty();
		}
	});

	return ScraperView;
});