define([
	'backbone',
	'models/Selection'
], function(
	Backbone,
	Selection
){
	var SelectionView = Backbone.View.extend({
		initialize: function(){
			_.bindAll(this);
			//this.model.on('change:html', this.updateSelections);
			this.selections = this.model.get('selections');
			//this.selections.on('reset', this.updateSelections);

			rangy.init();
			$(document).on('mouseup', ".body", this.checkSelection);
			$(document).on('click', ".marked", this.removeSelection);
			$(document).on('mouseover', ".marked", this.showTooltip);
			$(document).on('mouseout', ".marked", this.hideTooltip);
		},
		
		showTooltip: function(evt){
			var id = this.getIdFromClass(evt.target);
			var selection = this.selections.get(id);
			var last = $('.tmp_'+id+':first');
			var pos = last.position();
			app.tooltip.trigger('show', pos.top, selection);
		},
		
		hideTooltip: function(evt){
			app.tooltip.trigger('hide');			
		},
		
		checkSelection: function(){
			var selection = rangy.getSelection();
			if(selection.isCollapsed == false){
				selection.expand("word", { wordRegex: /[^\.a-z0-9]+(['\-][a-z0-9]+)*/gi });
				var html = selection.getRangeAt(0).toHtml();
				
				var div = $("<div>"+html+"</div>");
				if(div.find('span').length == 0){
					randId = Math.round(Math.random()*10000);
					var randClass = "marked"+randId;
					var cssApplier = rangy.createCssClassApplier(randClass, {normalize: true});
					cssApplier.applyToSelection();
										
					$('.'+randClass)
						.addClass('marked')
						.removeClass(randClass)
						.data('id', randId);
					
					var regex = new RegExp(/^<[^>]*>/);
					html = html.replace(regex, "");
					var regex = new RegExp(/<[^>]*>$/);
					html = html.replace(regex, "");

					var selectionModel = new Selection();
					selectionModel.save({
							article_id: this.model.get('id'),
							html: html
						},
						{ success: function(selection) { 
							$(".marked").filter(function() {
								if ($.data(this, "id") == randId) {
									$(this).addClass("tmp_" + selection.get('id') );
								}
							});
						}
					});
					this.selections.add(selectionModel);
				}
			}
			selection.removeAllRanges();
		},
		
		removeSelection: function(evt){
			evt.preventDefault();
			evt.stopPropagation();
			var id = this.getIdFromClass(evt.target);
			this.removeSelectionById(id);
			$('.tmp_'+id).contents().unwrap();
		},
		
		removeSelectionById: function(id) { 
			var model = this.selections.get(id);
			model.collection.remove(model);
			model.destroy();
			app.tooltip.trigger('hide');
		},
		
		getIdFromClass: function(element) {
			var classList = $(element).attr('class').split(/\s+/);
			for(var i=0; i<classList.length; i++) {
				name = classList[i];
				var split = name.split('_');
				if(split[0] == 'tmp') { 
					id = split[1];
					return id;
				}
			}
			return false;
		},
		
		updateSelections: function(){
			var that = this;	
			$.each( $('.body').find('.marked'), function ( index, selection ) {
				var selId = that.getIdFromClass(selection);
				if ($('.tmp_'+id).length > 1 ) {
					var startNode = selection;
					var endNode = selection;
					_.each( $('.tmp_'+id), function(part){
						endNode = part;
					});
					var range = rangy.createRange();
					range.setStartBefore(startNode);
					range.setEndAfter(endNode);
					var newString = range.toString();
					var model = that.selections.get(id);
					var oldString = model.get('selection');
					var newHtml = range.toHtml();
					var $tmp = $(newHtml);
					$tmp.find('.marked').each(function(index, content) {
						newHtml = newHtml.replace(content.outerHTML, $(content).html());
					});
					
					if(oldString !== newString) {
						var regex = new RegExp(/^<[^>]*>/);
						newHtml = newHtml.replace(regex, "");
						var regex = new RegExp(/<[^>]*>$/);
						newHtml = newHtml.replace(regex, "");
						model.set('html', newHtml);
					}
				}
				$(selection).contents().unwrap();

			});		
			
			var html = $('.body').html();
			if ( html !== undefined ) {
				this.selections.each(function(find){
					var id = find.get('id');
					var part = find.get('html');
					part = part.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
					var regex = new RegExp("("+part+")" , "gi" );
					html = html.replace(regex, '<span class="marked start tmp_'+id+'"></span>$1<span class="marked end tmp_'+id+'"></span>');					
				});

				$(".body").html($.trim(html));
				_.each( $('.marked.start'), function( startNode ){
					var id = this.getIdFromClass(startNode);
					if(id){
						var endNode = $('.marked.end.tmp_'+id);
						var range = rangy.createRange();
						range.setStartAfter(startNode);
						range.setEndBefore(endNode.get(0));
						var cssApplier = rangy.createCssClassApplier("marked tmp_"+id, {normalize: true});
						cssApplier.applyToRange(range);
					}
				}, this);
				$('.marked.start, .marked.end').remove();
			}
		},
		
		empty: function(){
			$(document).off('mouseup', ".body");
			$(document).off('click', ".marked");
			$(document).off('mouseover', ".marked");
			$(document).off('mouseout', ".marked");
			this.off();
			this.$el.empty();
		}
	});
	
	return SelectionView;
});