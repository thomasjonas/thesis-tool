define([
	'backbone',
	'underscore',
	'templates',
	'views/Article'
],

function (Backbone, _, templates, ArticleView) {
	"use strict";
	
	var ArticleList =  Backbone.View.extend({
		el: $("#sidebar"),
		template : templates.ArticleList,

		events : {
			'click' : 'viewArticle'
		},

		initialize : function(){
			_.bindAll(this, 'reset', 'render', 'add', 'remove');
			this.collection.bind('add', this.add);
			this.collection.bind('destroy', this.remove);
			this.collection.bind('reset', this.reset);
			this._articleViews = {};
			this.reset();
			this.render();
		},

		render: function(){
			this.$el.html(this.template());
			var $articles = this.$el.find(".articles");
			$articles.empty();
			for(var cid in this._articleViews){
				var view = this._articleViews[cid];
				$articles.append(view.$el);
			}
			return this;
		},

		add: function(article){
			var article_view = new ArticleView({model: article});
			this._articleViews[article.cid] = article_view;
			this.render();
		},
		
		remove: function(article){
			var oldView = this._articleViews[article.cid];
			oldView.remove();

		},

		reset: function(){
			this._articleViews = [];
			this.collection.each(function(article){
				this.add(article);
			}, this);
			this.render();
		},

		viewArticle : function(e){
			if(e.target.tagName !== "A"){
				e.preventDefault();
				$(e.target).find('a').trigger('click');
			}
		}
	});
	
	return ArticleList;
});