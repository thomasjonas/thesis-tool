define([
	'backbone',
	'models/Selection',
	'models/Tag',
	'templates'
], 
function (Backbone, Selection, Tag, templates) {
	"use strict";

	var TooltipView = Backbone.View.extend({
		className: "tooltip",
		template: templates.TooltipView,
		
		events: {
			'mouseover' : "show",
			'mouseout' : "hide",
			'keypress #tag_input' : "enter",
			'click span' : "removeTag"
		},

		initialize: function(){
			_.bindAll(this);
			this.model = new Selection();
			$("body").append(this.$el);
			this.on('show', this.show);
			this.on('hide', this.hide);
			this.render();
		},

		render: function(){
			if(this.selection !== undefined) {
				var tags = this.selection.get('tags');
				this.$el.html(this.template({tags: tags.toJSON()}));
			} else {
				this.$el.html(this.template({tags: {}}));
			}
			this.doAutoComplete();
			return this;
		},
		
		removeTag: function(event){
			var id = $(event.target).data('id');			
			var tags = this.selection.get('tags');
			var tag = tags.get(id);
			tags.remove(tag);
			this.render();
		},
		
		doAutoComplete: function(){
			var that = this;
			this.$el.find("#tag_input").autocomplete({ 
				source: function(request, response) {
					var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
					response( $.grep( app.tags.toJSON(), function( value ) {
						return matcher.test( value.value );
					}));
				},
				select: function( event, ui ) {
					$(event.target).val(ui.item.value).prev().val(ui.item.id);
					that.submit();
					return false;
				} 
			}).data( "autocomplete" )._renderItem = function( ul, item ) {
				return $( "<li>" )
				.data( "item.autocomplete", item )
				.append( "<a>" + item.value + "</a>" )
				.appendTo( ul );
			};
		},
		
		show: function(top, selection) {
			if(selection !== undefined) {
				this.selection = selection;
				var tags = this.selection.get('tags');
				tags.on('add', this.render);
				this.render();
			}
			clearTimeout(this.timer);
			var pos = $('.story').position();
			var left = pos.left;
			this.$el.css({ top: top, left: left - 185 });
			this.$el.addClass('visible');			
		},
		
		hide: function(event) {
			clearTimeout(this.timer);
			var that = this;
			this.timer = setTimeout(function(){
				that.$el.removeClass('visible');			
			}, 200)
		},
		
		enter: function(event) {
			if (event.keyCode == 13 && event.target.value !== undefined){
				this.submit();
			}			
		},
		
		submit: function() {
			console.log('submit');
			var id = $("#tag_id").val();
			var value = $.trim( $("#tag_input").val() );
			var t = new Tag();
			if( id !== undefined && id !== "") {
				t = app.tags.get(id);
			} else {
				t = app.tags.where({value: value})[0];
				if( t == undefined ) {
					t = new Tag({
						value: value
					});
				}
				app.tags.add(t);
			}
			t.set('selectionId', this.selection.get('id'));
			var tags = this.selection.get('tags');
			tags.add(t);
			t.save();
			t.unset('selectionId');
			$("#tag_input").val("");
			$("#tag_id").val("");
		}
	});

	return TooltipView;
});