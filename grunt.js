/**
	NodeJS
	GruntJS
**/

module.exports = function(grunt) {
	grunt.initConfig({
		// The clean task ensures all files are removed from the dist/ directory so
	    // that no files linger from previous builds.
		clean: ["dist/"],
		
		// The lint task will run the build configuration and the application
		// JavaScript through JSHint and report any errors.  You can change the
		// options for this task, by reading this:
		// https://github.com/cowboy/grunt/blob/master/docs/task_lint.md
		lint: {
			files: [
				"build/config.js", "app/**/*.js"
				]
		},
		
		// This task uses James Burke's excellent r.js AMD build tool.  In the
		// future other builders may be contributed as drop-in alternatives.
		requirejs: {
			// Include the main configuration file.
			mainConfigFile: "app/config.js",
		
			// Output file.
			out: "dist/debug/require.js",
		
			// Root application module.
			name: "config",
					
			// Do not wrap everything in an IIFE.
			wrap: false,
		
			// Build Handlebars runtime, instead of full version.
			paths: {
/* 				handlebars: "../assets/js/libs/handlebars.runtime-1.0.0.beta" */
/*
		        jquery: "empty:",
		        jqueryui: "empty:"
*/
			}

		},
		
		// Takes the built require.js file and minifies it for filesize benefits.
		min: {
			"dist/release/require.js": [
				"dist/debug/require.js"
			]
		},
		
		// Lists of files to be concatenated, used by the "concat" task.
		concat: {
			dist: {
				src: [
					"assets/js/libs/almond.js",
					"dist/debug/templates.js",
					"dist/debug/require.js"
				],
			
				dest: "dist/debug/require.js",
			
				separator: ";"
			}
		},
		
		// The stylus task is used to compile Stylus stylesheets into a single
		// CSS file for debug and release deployments.  
		stylus: {
			// Put all your CSS files here, order matters!
			files: {
				main: [ "assets/stylus/index.styl" ],
				editor: [ "assets/stylus/editor/editor.styl" ]
			},
					
			// Default task which runs in debug mode, this will build out to the
			// `dist/debug` directory.
			compile: {
				// Used for @imports.
				options: { 
					compress: true,
					paths: ["assets/css"] 
				},
		
				files: {
					"dist/debug/index.css": "<config:stylus.files.main>",
					"dist/debug/editor.css": "<config:stylus.files.editor>"
				}
			},
		
			// This dev task only runs with `watch:stylus` this will *completely*
			// overwrite the `assets/css/index.css` file referenced in `index.html`.
			dev: {
				// Used for @imports.
				options: { 
					compress: true,
					paths: ["assets/css"] 
				},
		
				files: {
					"assets/css/index.css": "<config:stylus.files.main>",
					"assets/css/editor.css": "<config:stylus.files.editor>"
				}
			}
		},
				
		// Lists of files to be minified with UglifyJS, used by the "min" task.
	    mincss: {
			"dist/release/index.css": [
				"dist/debug/index.css"
			]
		},
		
		handlebars: {
			"dist/debug/templates.js": ["app/templates/**/*.hbs"]
		},
		
		// Configuration options for the "watch" task.
		watch: {
			stylus: {
		        files: ["grunt.js", "assets/stylus/**/*.styl"],
		        tasks: "stylus:dev"
		    }			
		},
		// Global configuration options for UglifyJS.
		uglify: {},
		
	});
	
	grunt.loadNpmTasks('grunt-contrib-stylus');	
	grunt.loadNpmTasks('grunt-requirejs');	
	grunt.loadNpmTasks('grunt-contrib-mincss');
	grunt.loadNpmTasks('grunt-contrib-handlebars');
	grunt.loadNpmTasks('grunt-clean');
		
	// The debug task will remove all contents inside the dist/ folder, lint
	// all your code, precompile all the underscore templates into
	// dist/debug/templates.js, compile all the application code into
	// dist/debug/require.js, and then concatenate the require/define shim
	// almond.js and dist/debug/templates.js into the require.js file.
	grunt.registerTask("debug", "clean handlebars requirejs concat stylus:compile");
	
	// The release task will run the debug tasks and then minify the
	// dist/debug/require.js file and CSS files.
	grunt.registerTask("release", "debug min mincss");
};